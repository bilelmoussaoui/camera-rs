use gtk::glib;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use std::env::args;
mod camera_paintable;

use camera_paintable::CameraPaintable;

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(Debug)]
    pub struct Window {
        pub camera_paintable: CameraPaintable,
    }

    impl ObjectSubclass for Window {
        const NAME: &'static str = "Window";
        type Type = super::Window;
        type ParentType = gtk::ApplicationWindow;
        type Instance = subclass::simple::InstanceStruct<Self>;
        type Class = subclass::simple::ClassStruct<Self>;

        glib::object_subclass!();

        fn new() -> Self {
            let camera_paintable = CameraPaintable::new();

            Self { camera_paintable }
        }
    }

    impl ObjectImpl for Window {
        fn constructed(&self, obj: &Self::Type) {
            obj.setup_widgets();
            self.parent_constructed(obj);
        }
    }
    impl WidgetImpl for Window {}
    impl WindowImpl for Window {}
    impl ApplicationWindowImpl for Window {}
}

glib::wrapper! {
    pub struct Window(ObjectSubclass<imp::Window>) @extends gtk::Widget, gtk::Window, gtk::ApplicationWindow;
}

impl Window {
    pub fn new<P: glib::IsA<gtk::Application>>(app: &P) -> Self {
        glib::Object::new(&[("application", app)]).expect("Failed to create Window")
    }

    pub fn setup_widgets(&self) {
        let self_ = imp::Window::from_instance(self);
        self.set_title(Some("Camera"));
        self.set_default_size(720, 600);

        let box_ = gtk::Box::new(gtk::Orientation::Vertical, 24);

        let image = gtk::Picture::new();
        image.set_halign(gtk::Align::Center);
        image.set_valign(gtk::Align::Center);
        image.set_size_request(600, 480);
        image.set_paintable(Some(&self_.camera_paintable));

        box_.append(&image);

        self.set_child(Some(&box_));
    }
}

fn main() {
    gst::init().unwrap();
    let application = gtk::Application::new(Some("com.belmoussaoui.camera"), Default::default())
        .expect("Initialization failed...");

    application.connect_activate(|app| {
        let win = Window::new(app);
        win.show();
    });

    application.run(&args().collect::<Vec<_>>());
}
